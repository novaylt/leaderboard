# Scrabble Leaderboard
It is as the name suggests, a leaderboard that shows scores including win/ loss records between all players. This project does not contain the actual scrabble game but only stores scores.

# Contact Me
To know more about this project, email me ibrahim_345@outlook.com

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
