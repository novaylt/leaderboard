@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" align="center">

        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">


                    <div class="panel-body">

                     
                                        <h2><i class="fa fa-users"></i> ALL CANDIDATES</h2>

                                        <div class="table-responsive" >
                                            <table class="table table-bordered table-striped">


                      @if(Session::has('updatescore'))
                          <div class="alert alert-success">
                              {{ Session::get('updatescore') }}
                          </div>
                      @endif
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Joined On</th>
                                                        <th>Profile</th>
                                                        <th>Choose Opponent</th>
                                                    </tr>
                                                </thead>
                                                @foreach($Users as $key => $user)
                                                <tr>
                                                  <td>{{ $user->name }}</th>
                                                  <td>{{ $user->email }}</th>
                                                  <td>{{ $user->created_at }}</th>  
                                                   <td><a href ="candidate/{{$user->id}}" class ='btn btn-info'>View Profile</a></th> 
                                                   <td>
                                                            @if (Auth::guest())
                                                                You Must Be Logged In To Face An Opponent!
                                                            @elseif(Auth::user()->name == $user->name)
                                                                
                                                                Can't Play Yourself!

                                                            @elseif(Auth::user()->name != $user->name)
                                                            <a href ="scorescreen/{{$user->id}}" class ='btn btn-danger'>Play Against</th>
                                                            @endif
                                                <tr>                            
                                                @endforeach
                                              </table>

                                    {!! $Users->render() !!} 

                                        </div>

                      </div>
             </div>
        </div>


                       </div>
         

</div>
</div>
</div>


    </div>
</div>
@endsection
