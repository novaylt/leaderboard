@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" align="center">
         <div class="col-md-12 col-md-offset-0">
          

                    <div class="panel panel-default">

                    <h1><i class="fa fa-info-circle"></i> SCRABBLE LEADER DASHBOARD</h1>

   					</div>



   				  @if(Auth::guest())

 										  <div class="col-md-3 col-md-offset-1">
						                       <a href="{{ url('top10scores') }}"><img class="img-zoom img-responsive" src="images/top10.png" alt="Top 10"/></a>
						                  </div>

						  				  <div class="col-md-3 col-md-offset-1">
						                       <a href="{{ url('candidates') }}"><img class="img-zoom img-responsive" src="images/candidates.png" alt="Candidates"/></a>
						                  </div>

						                  <div class="col-md-3 col-md-offset-1">
						                       <a href="{{ url('contact') }}"><img class="img-zoom img-responsive" src="images/contact.png" alt="Contact Admin"/></a>
						                  </div>

                  @elseif (Auth::guest() OR Auth::user()->admin!=1)

 										  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('top10scores') }}"><img class="img-zoom img-responsive" src="images/top10.png" alt="Top 10"/></a>
						                  </div>

 										  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('candidates') }}"><img class="img-zoom img-responsive" src="images/candidates.png" alt="Candidates"/></a>
						                  </div>

						                  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('profile') }}"><img class="img-zoom img-responsive" src="images/profile.png" alt="Profile"/></a>
						                  </div>

						                  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('/logout') }}"><img class="img-zoom img-responsive" src="images/logout.png" alt="Logout"/></a>
						                  </div>


					@elseif (Auth::user()->admin==1)
						 				  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('top10scores') }}"><img class="img-zoom img-responsive" src="images/top10.png" alt="Top 10"/></a>
						                  </div>

						  				  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('candidates') }}"><img class="img-zoom img-responsive" src="images/candidates.png" alt="Candidates"/></a>
						                  </div>

 										  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('manageusers') }}"><img class="img-zoom img-responsive" src="images/manageusers.png" alt="Users"/></a>
						                  </div>

						                  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('messages') }}"><img class="img-zoom img-responsive" src="images/messages.png" alt="Messages"/></a>
						                  </div>			
				
						                  <div class="col-md-3 col-md-offset-3">
						                       <a href="{{ url('profile') }}"><img class="img-zoom img-responsive" src="images/profile.png" alt="Profile"/></a>
						                  </div>

						                  <div class="col-md-3 col-md-offset-0">
						                       <a href="{{ url('/logout') }}"><img class="img-zoom img-responsive" src="images/logout.png" alt="Logout"/></a>
						                  </div>
					@endif             
     
    </div>
</div>

    </div>
</div>
@endsection
