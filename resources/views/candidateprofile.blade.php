@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" align="center">

        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">


                    <div class="panel-body">
                  
                                         @foreach($candidateprofile as $profileinfo)
                                        <h2><i class="fa fa-users"></i> {{ $profileinfo->profileowner }}'s Profile</h2>
                                          @endforeach
                                        <div class="table-responsive" >
                                            <table class="table table-bordered table-striped">

                                                <thead>
                                                    <tr>
                                                        <th>Win/Loss</th>
                                                        <th>Candidate Score</th>
                                                        <th>Opponent</th>
                                                        <th>Opponent Score</th>
                                                        <th>Achieved On</th>         
                                                    </tr>
                                                </thead>
                          @if (count($winloss)) 
                                               @foreach($winloss as $candidateinfo)
                                                <tr>
                                                  <td> @if($candidateinfo->wins==0 && $candidateinfo->losses==1) 
                                                        <a class ='btn btn-danger'>Loss</a>
                                                        @elseif($candidateinfo->losses==0 && $candidateinfo->wins==1) 
                                                        <a class ='btn btn-info'>Win</a></th>
                                                         @endif </th>                                             
                                                  <td> {{ $candidateinfo->candidatescore }}</th> 
                                                  <td><a href ="/candidate/{{$candidateinfo->opponentid}}"> {{ $candidateinfo->opponentname }}</th>
                                                  <td> {{ $candidateinfo->opponentscore }}</th>         
                                                  <td> {{ $candidateinfo->achievedat }}</th>                                          
                                                <tr>                            
                                                 @endforeach

                           @else  
                              <div align="center"><p><strong><h3>Sign In And Be The First To Challenge This Person!</h3></strong></p></div>
                           @endif 

                                              </table>

                                              {!! $winloss->render() !!}  

                                        </div>

                    <button class="btn btn-primary" onclick="history.go(-1)">
                      « Return Back
                    </button>

        </div>
    </div>
</div>
</div>
</div>
@endsection
