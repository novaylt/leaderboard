@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" align="center">

                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">

                    <div class="panel-body">

                    
                      @if (count($errors) > 0)
                          <div class="alert alert-danger">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div>
                      @endif

                @if(Session::has('errormessage'))
                    <div class="alert alert-error">
                        {{ Session::get('errormessage') }}
                    </div>
                @endif

                      <h2><i class="fa fa-star" aria-hidden="true"></i></i> SCORE SCREEN</h2>
                      <br></br>



                      @foreach ($opponentinfo as $opponentdata)
                      <h4><i class="fa fa-user"></i> Your Opponent: {{ $opponentdata->opponentname }}</h2>
                      @endforeach

                    
                    <br></br>

                      {!! Form::open() !!}
                  
                      <!-- Title form input -->
               

                                <div class="form-group">
                                    {!! Form::label('result', 'What did you attain:') !!}
                                 {!! Form::select('result', ['Choose an option' => "", '1' => 'Win', '0' => 'Loss']) !!}
                                </div>
                      <br></br>
                                <div class="form-group">
                                    {!! Form::label('candidatescore', 'What is your Score:') !!}
                                    {!! Form::text('candidatescore', null, ['class' => 'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('opponentscore', 'What is your opponents Score:') !!}
                                    {!! Form::text('opponentscore', null, ['class' => 'form-control']) !!}
                                </div>
           
                              {{ Form::submit('SAVE', array('class' => 'btn btn-info')) }}

                              {!! Form::close() !!}

                              </div>


                    </div>

       <button class="btn btn-danger" onclick="history.go(-1)">
                      WITHDRAW
       </button>

                </div>


            </div>

    </div>
</div>
@endsection
