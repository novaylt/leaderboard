@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" align="center">

        <div class="col-md-3 col-md-offset-0">
            <div class="panel panel-default">

                    <div class="panel-heading"><i class="fa fa-info-circle"></i>  Top 10 Wins Achieved</div>
                    <div class="panel-body"> <div class="table-responsive">
                            <table class="table table-bordered table-striped">
         
                                <thead>
                                    <tr>
                                        <th>Candidate</th>
                                        <th>Wins</th>
                                        
                                    </tr>
                                </thead>
                              @foreach($TopWins as $Top10Wins)    
                                <tr>
                                  <td><a href ="/candidate/{{$Top10Wins->candidateid}}">{{ $Top10Wins->candidatename }}</th>
                                  <td>{{ $Top10Wins->totalwins }}</th>
                                </tr>
                            
                              @endforeach
                              </table>

                        </div>      
                    </div>
                </div>
        </div>


<div class="col-md-3 col-md-offset-0">
            <div class="panel panel-default">

                    <div class="panel-heading"><i class="fa fa-info-circle"></i>  Top 10 Losses Achieved</div>
                    <div class="panel-body"> <div class="table-responsive">
                            <table class="table table-bordered table-striped">
         
                                <thead>
                                    <tr>
                                        <th>Candidate</th>
                                        <th>Losses</th>
                                        
                                    </tr>
                                </thead>
                              @foreach($TopLosses as $Top10Losses)    
                                <tr>
                                  <td><a href ="/candidate/{{$Top10Losses->candidateid}}">{{ $Top10Losses->candidatename }}</th>
                                  <td>{{ $Top10Losses->totallosses }}</th>
                                </tr>
                            
                              @endforeach
                              </table>

                        </div>      
                    </div>
                </div>
        </div>


<div class="col-md-3 col-md-offset-0">
            <div class="panel panel-default">

                    <div class="panel-heading"><i class="fa fa-info-circle"></i>  Top 10 High Scores Achieved</div>
                    <div class="panel-body"> <div class="table-responsive">
                            <table class="table table-bordered table-striped">
         
                                <thead>
                                    <tr>
                                        <th>Candidate</th>
                                        <th>Score</th>
                                        
                                    </tr>
                                </thead>
                              @foreach($TopHighestScores as $Top10Scores)    
                                <tr>
                                  <td><a href ="/candidate/{{$Top10Scores->candidateid}}">{{ $Top10Scores->username }}</th>
                                  <td>{{ $Top10Scores->totalhighestscore }}</th>
                                </tr>
                            
                              @endforeach
                              </table>

                        </div>      
                    </div>
                </div>
        </div>


<div class="col-md-3 col-md-offset-0">
            <div class="panel panel-default">

                    <div class="panel-heading"><i class="fa fa-info-circle"></i>  Top 10 Lowest Scores Achieved</div>
                    <div class="panel-body"> <div class="table-responsive">
                            <table class="table table-bordered table-striped">
         
                                <thead>
                                    <tr>
                                        <th>Candidate</th>
                                        <th>Score</th>
                                        
                                    </tr>
                                </thead>
                              @foreach($TopLowestScores as $Top10LowestScores)    
                                <tr>
                                  <td><a href ="/candidate/{{$Top10LowestScores->candidateid}}">{{ $Top10LowestScores->username }}</th>
                                  <td>{{ $Top10LowestScores->totallowestscore }}</th>
                                </tr>
                            
                              @endforeach
                              </table>

                        </div>      
                    </div>
                </div>
        </div>


    </div>
</div>
@endsection
