<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWinlossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('winloss', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('wins');
            $table->integer('losses');
            $table->integer('candidatescore');
            $table->integer('opponentscore');
            $table->integer('opponent_id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('winloss');
    }
}
