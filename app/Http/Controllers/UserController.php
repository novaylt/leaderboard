<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\WinLossModel;
use DB;
use Validator;
use Input;
use Auth;
use Redirect;
use Session;

class UserController extends Controller
{
    protected $users;

// Method for retrieving list of users on manageusers  
    public function GetUsers()
    {
      $Users = \App\User::paginate(10);
	  return view('candidates')->with('Users',$Users);
    }




    // Method for retrieving candidate info candidateprofile view
    public function showuserinfo($id)
    {
     $candidateinfo = User::find($id);
	   $candidateprofile = DB::table('users')     
        ->select('users.name as profileowner')    
        ->where('users.id', '=', $id)              
        ->get();

     $winloss = DB::table('winloss')     
        ->select('winloss.id as winlossid', 'winloss.candidate_id as candidateid', 'winloss.candidate_id as candidateid', 'winloss.wins as wins', 'winloss.losses as losses', 'winloss.candidatescore as candidatescore', 'winloss.opponentscore as opponentscore', 'winloss.opponent_id as opponentid', 'users.name as opponentname', 'winloss.created_at as achievedat')    
        ->join('users', 'users.id', '=', 'winloss.opponent_id')
        ->where('winloss.candidate_id', '=', $id)
        ->orderBy('winloss.created_at', 'DESC')              
        ->paginate(10);


        return view('candidateprofile')
           ->with ('candidateinfo', $candidateinfo)
           ->with ('candidateprofile', $candidateprofile)
           ->with ('winloss', $winloss);
    }



    public function playopponent($id){
    if($id==Auth::id()) 
      { 
        return Redirect::back();
      }
     else 
      {
         $data = User::findOrFail($id);
      	 $opponentinfo = DB::table('users')     
        ->select('users.name as opponentname')    
        ->where('users.id', '=', $id)              
        ->get();

      return view('scorescreen')
      ->with('data', $data)
      ->with('opponentinfo', $opponentinfo);
      }
    }

// Method for storing contact message to database
  public function storescore($id, Request $request)
  {
          $rules = array(
          'result' => 'required',
          'candidatescore' => 'required|integer',
          'opponentscore' => 'required|integer'
        );
        $validator = Validator::make(Input::all(), $rules);

       if ($validator-> fails())
              {
                return redirect('scorescreen')
                ->withErrors($validator)
                ->withInput();
              }
       else
            {
              $savecandidatescore = new \App\WinLossModel;
              $savecandidatescore->candidate_id = Auth::id();

              if(Input::get('result')==1)
              {
              	 $savecandidatescore->wins = 1;
              	 $savecandidatescore->losses = 0;
              }
              else
              {
              	 $savecandidatescore->wins = 0;
              	 $savecandidatescore->losses = 1;
              }	
                        
              $savecandidatescore->candidatescore = Input::get('candidatescore'); 
              $savecandidatescore->opponentscore = Input::get('opponentscore');        
              $savecandidatescore->opponent_id = $id;
              $savecandidatescore -> save();


            
              $saveopponentscore = new \App\WinLossModel;
              $saveopponentscore->candidate_id = $id;

              if(Input::get('result')==1)
              {
                 $saveopponentscore->wins = 0;
                 $saveopponentscore->losses = 1;
              }
              else
              {
                 $saveopponentscore->wins = 1;
                 $saveopponentscore->losses = 0;
              } 
                        
              $saveopponentscore->candidatescore = Input::get('opponentscore'); 
              $saveopponentscore->opponentscore = Input::get('candidatescore');  
              $saveopponentscore->opponent_id = Auth::id();
              $saveopponentscore -> save();


              Session::flash('updatescore', 'Score Saved!');
              return Redirect::to('candidates');
            } 

    }



  public function gettop10()
  {
    $TopWins = DB::table('winloss')     
                ->select('winloss.candidate_id as candidateid', 'users.name as candidatename')
                ->selectRaw('winloss.*, sum(winloss.wins) as totalwins')
                ->join('users', 'users.id', '=', 'winloss.candidate_id')  
                ->groupBy('candidatename')
                ->orderBy('totalwins', 'DESC')  
                ->take(10)
                ->get();
   $TopLosses = DB::table('winloss')     
                ->select('winloss.candidate_id as candidateid', 'users.name as candidatename')
                ->selectRaw('winloss.*, sum(winloss.losses) as totallosses')
                ->join('users', 'users.id', '=', 'winloss.candidate_id')  
                ->groupBy('candidatename')
                ->orderBy('totallosses', 'DESC')  
                ->take(10)
                ->get(); 
    $TopHighestScores = DB::table('winloss')     
                ->select('users.id as userid' , 'winloss.candidate_id as candidateid', 'users.name as username')
                ->selectRaw('winloss.*, MAX(winloss.candidatescore) as totalhighestscore')
                ->join('users', 'users.id', '=', 'winloss.candidate_id')  
                ->groupBy('candidate_id')
                ->orderBy('totalhighestscore', 'DESC')  
                ->take(10)
                ->get();  
    $TopLowestScores = DB::table('winloss')     
                ->select('users.id as userid' , 'winloss.candidate_id as candidateid', 'users.name as username')
                ->selectRaw('winloss.*, MIN(winloss.candidatescore) as totallowestscore')
                ->join('users', 'users.id', '=', 'winloss.candidate_id')  
                ->groupBy('candidate_id')
                ->orderBy('totallowestscore', 'ASC')  
                ->take(10)
                ->get();  
    return view('top10scores')
          	->with('TopWins',$TopWins)
            ->with('TopLosses',$TopLosses)
            ->with('TopHighestScores',$TopHighestScores)
            ->with('TopLowestScores',$TopLowestScores);
           
  }
}
