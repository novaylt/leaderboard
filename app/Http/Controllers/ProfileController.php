<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;

class ProfileController extends Controller

{
       /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

// Method for retrieving user profile         
    public function show()
    {
        {
            $profileinfo = User::find(Auth::user()->id);
           
             return view('profile')->with ('profileinfo', $profileinfo);
        }
    }
}