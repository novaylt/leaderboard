<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => 'web'], function () {
				Route::group(['middleware' => 'admin'], function () {

							// Messages route, connected controllers and methods
							Route::get('messages', function () {
							return view('messages');
							});

							Route::get('messages', 'ContactController@GetMessages');
							Route::delete('messages/delete/{id}',array('uses' => 'ContactController@destroy', 'as' => 'messages'));
							Route::get('messages/{id}', 'ContactController@showmessagecontents');

			            // RoleMiddleware ends here

			  	 });




		Route::auth();

		// Profile route, connected controllers and methods
		Route::get('profile',array('uses' => 'ProfileController@show', 'as' => 'profile')); 

		Route::get('scorescreen', function () {
		  return abort(503);
		});

		  Route::get('scorescreen/{id}',array('uses' => 'UserController@playopponent', 'as' => 'scorescreen'))->middleware(['auth', 'web']);
		  Route::post('scorescreen/{id}',array('uses' => 'UserController@storescore', 'as' => 'scorescreen'))->middleware(['auth', 'web']);

			
		 // Web Middleware ends here

		 });        




Route::get('/top10scores', function () {
    return view('top10scores');
});

Route::get('top10scores', 'UserController@gettop10');


// Contact Admin Route, connect controller and methods
Route::get('/contact', function () {
return view('contact');
});

Route::post('contact', 'ContactController@store');


 // Manage Users routes, connected controller and methods
Route::get('candidates', function () {
return view('candidates');
});

Route::get('candidates', 'UserController@GetUsers');
Route::get('candidate/{id}', 'UserController@showuserinfo');


