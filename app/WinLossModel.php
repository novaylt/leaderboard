<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WinLossModel extends Model
{
   protected $table = 'winloss';
   protected $fillable = array('id', 'candidate_id', 'wins', 'losses', 'candidatescore', 'opponentscore', 'opponent_id');
}
